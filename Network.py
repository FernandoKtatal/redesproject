import Util as Util
from Constants import *

class Network(object):

    def __init__(self, ip):
        Util.log(self, "Network layer has started", WARNING)
        self.ip = ip

    def route(self, ip, message):
        if(''.join(ip) == self.ip):
            Util.log(self,"[{}] Message {} received".format(type(self).__name__,''.join(message)), SUCCESS)
        else:
            Util.log(self,"[{}] Message {} discarded".format(type(self).__name__,''.join(message)), FAIL)
